'''
@file       lab1main.py
@brief      LED button press finite state machine
@details    This file will run through various states that cycle through 3 different LED cylce variations at a button press.
@author     Esmeralda Gonzalez
@date       October 3, 2021 
'''

#imports near the top
import pyb
import utime 
import math

## Constant for the initial button press before any waves are shown
initialButtonPress = 0
## Constant for changing states 
needsToChange = 0


def onButtonPressFCN(IRQ_src):
    '''
    @brief                  Button Press Handler
    @details                This function will change the global variable to 1 indicating that the state needs to change
    @param IRQ_src          Default interrupt paramter (not used)        
    '''
    global needsToChange
    needsToChange = 1

def update_STW(current_Time):
    '''
    @brief                  Saw Tooth Wave function
    @details                This function will calculate the saw tooth wave value based on the current time.
    @param current_Time     The current duration of time that has passed since Sawtooth wave began.
    @return                 The brightness percentage level that the LED should be on based on the wave and time. 
    '''
    return 100*(current_Time%1.0)

def update_SQW(current_Time):
    '''
    @brief                  Sqaure Wave function
    @details                This function will calculate the square wave value based on the current time.
    @param current_Time     The current duration of time that has passed since Square wave began.
    @return                 The brightness percentage level that the LED should be on based on the wave and time. 
    '''
    return 100*(current_Time%1.0<0.5)

def update_Sine(current_Time):
    '''
    @brief                  Sine Wave function 
    @details                This funciton will calculate the sine wave value based on the current time.
    @param current_Time     The current duration of time that has passed since Sine wave began.
    @return                 The brightness percentage level that the LED should be on based on the wave and time. 
    '''
    return 100 * (0.5*math.sin(current_Time) + 0.5)

def reset_Timer():
    '''
    @brief                  New Current Time
    @details                This will get the current time at which the function is called and return it to reset
                            the start time that for each wave when the button is pressed.
    @return                 The current time in milliseconds.
    '''
    return utime.ticks_ms()

def timeElapsedinSeconds(startTime):
    '''
    @brief                  Get Duration of Time 
    @details                Taking a start time from when a wave started it'll get the current time and calculate
                            the duration that has been since beginning to determine the brightness level.
    @param startTime        The start time in milliseconds that the wave started at
    @return                 The duration of time that has passed but converted to seconds
    '''
    current = utime.ticks_ms()
    duration = utime.ticks_diff(current, startTime)
    
    return duration / 1000

if __name__=='__main__':
    ##constant initial values
    state = 0
    runs = 0
    startTime = 0
    t2ch1 = 0
    ## A set of states that run to display various waves as LED lights on Nucleo
    while (True):
        try:
            if (state==0):
                ## Running State zero where it initializes user input
                # Defines pins on Nucleo 
                LED2 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
                button = pyb.Pin (pyb.Pin.cpu.C13)
                ButtonInt = pyb.ExtInt(button, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=LED2)
                print('Welcome to lab 1.')
                print('Press button B1 to cycle through LED patterns on LED2.')
                state = 1
            elif (state==1):
                ##Running State one that waits for any user input that will intiate state 2
                if needsToChange:
                    print('Square Wave Pattern Selected')
                    needsToChange = 0
                    startTime = reset_Timer()
                    state = 2
            elif (state==2):
                ##Running State two that runs the square wave function in real time
                brightness = update_SQW(timeElapsedinSeconds(startTime))
                #print("Brightness: ",brightness)
                t2ch1.pulse_width_percent(brightness)
                
                if needsToChange:
                    print('Sine Wave Pattern Selected')
                    needsToChange = 0
                    startTime = reset_Timer()
                    state = 3
            elif (state==3):
                ##Running State 3 that runs the sine wave function in real time
                brightness = update_Sine(timeElapsedinSeconds(startTime))
                t2ch1.pulse_width_percent(brightness)
                #print('Running State 3')
                if needsToChange:
                    print('Sawtooth Wave Pattern Selected')
                    needsToChange = 0
                    startTime = reset_Timer()
                    state = 4
            elif (state==4):
                ##Running State 4 that runs the saw tooth wave in real time 
                brightness = update_STW(timeElapsedinSeconds(startTime))
                t2ch1.pulse_width_percent(brightness)
                if needsToChange:
                    print('Square Wave Pattern Selected')
                    needsToChange = 0
                    startTime = reset_Timer()
                    state = 2
                
            runs += 1
        except KeyboardInterrupt:
            break

        
    print('Program Terminating')
    