'''
@file       encoder_gonzalez.py
@brief      Encoder Driver
@details    Encoder driver that creates a timer and timer channels for given pins that an encoder is connected to.
@author     Esmeralda Gonzalez
@date       October 17, 2021 
'''

import pyb

    
class Encoder:
    '''
    Encoder Driver class that sets up an encoder with update and get methods
    '''
    def __init__(self, pin1, pin2, timer):
        '''
        @brief      Initialize function
        @details    Sets up an encoder driver with given pins and timer to use update and get methods for data retrieval
        @param      pin1    First pin of encoder
        @param      pin2    Second pin of encoder
        @param      timer   Timer that is user for the encoder
        '''
        self.timer = timer
        self.timerch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        self.timerch2 = self.timer.channel (2, pyb.Timer.ENC_AB, pin=pin2)
        self.position = 0
        self.previous = 0
        self.delta = 0
        
        print("creating encoder object")
    
    def update(self):
        ''' 
        @brief      Update Encoder Class Variables
        @details    Updates the encoder variables of this class with current position of the encoder and also accounting for over/underflow
        '''
        current = self.timer.counter()
        self.delta = current - self.previous
        
        if self.get_delta() > (65535//2):
            self.delta -= 65535
        elif self.get_delta() < (-65535//2):
            self.delta += 65535
            
        self.set_position(self.get_position() + self.get_delta())
        self.previous = current
        print("Reading encoder count and updating position and delta value")
    
    def get_position(self):
        '''
        @brief      Get position function
        '''
        return self.position
    
    def set_position(self, position):
        '''
        @brief      Set position function
        @param      position    position to set the encoder to
        '''
        self.position = position
        
    def get_delta(self):
        '''
        @brief      Get encoder delta function
        '''
        return self.delta

