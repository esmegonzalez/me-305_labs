'''
@file       main_gonzalez.py
@brief      Main Driver Code
@details    Main file that handles running the user interface and encoder tasks
@author     Esmeralda Gonzalez
@date       October 17, 2021 
'''

# imports
from Shared import Shared, Queue
from task_encoder_gonzalez import Task_Encoder
from task_user_gonzalez import Task_User
from encoder_gonzalez import Encoder

def main():
    ''' 
    @details    Driver code to make encoder task and UI task work together
    '''

    # create shared command and encoder variables for communication
    command = Shared("")
    encoder1 = Shared(setupEncoder1())

    # create task objects with period and shared variables
    encoder_user = Task_User(10000, command, encoder1)
    encoder_task = Task_Encoder(1000, command, encoder1)

    while True:
        try:
            # keep running tasks simultaneously
            encoder_task.run()
            encoder_user.run()
        # if keyboard interrupt encountered exit cleanly
        except KeyboardInterrupt:
            break

    print("Done")

def setupEncoder1():
    ''' 
    @brief      Create Encoder 1 
    @return     Encoder object that will be used as shared variable
    '''
    ch1 = pyb.Pin (pyb.Pin.board.PB6, pyb.Pin.IN)
    ch2 = pyb.Pin (pyb.Pin.board.PB7, pyb.Pin.IN) 
    tim4 = pyb.Timer(4, prescaler=0, period=65535)
    return Encoder(ch1, ch2, tim4)

def setupEncoder2():
    pass

if __name__ == '__main__':
    main()
