'''
@file       task_encoder_gonzalez.py
@brief      Updating Encoder Task
@details    Periodically updates encoder based on the period that is given
@author     Esmeralda Gonzalez
@date       October 17, 2021 
'''

# imports
import utime
import encoder_gonzalez
import pyb

# STATES
S0_INIT = 0
S1_UPDATE = 1

class Task_Encoder:
    ''' Encoder task that periodically updates the encoder based on the period that is given.'''

    def __init__(self, period, command, encoder):
        '''
        @brief      Initialize Function            
        @details    Initializes class with starting state, how often to update based on period, has two shared variables that is used to get commands from UI
        @param      period      The cycle period to update the encoder in microseconds
        @param      command     Command received from UI
        @param      encoder     The encoder object that is connected to the PINs on the board     
        '''
         
        self.state = S0_INIT
        self.runs = 0
        self.period = period
        self.next_time = utime.ticks_us() + period
        
        self.encoder = encoder
        self.command = command
        
    def run(self):
        '''
        @brief      Periodic Encoder Update
        @details    Function should be ran in a while loop to periodically update the encoder and also check if a "z" command has been entered from UI to zero it.
        '''
        self.runs += 1
        # check if period has been reached and needs to be updated
        if utime.ticks_us() >= self.next_time:
            # initialize state for setup
            if self.state == S0_INIT:
                #RUN STATE 0
                self.state == S1_UPDATE
            
            # Update state for calling the encoder update method to update its current position and delta
            if self.state == S1_UPDATE:
                # if zero command then set the position of encoder to 0
                if self.command == "z":
                    self.encoder.set_position(0)
                    self.command = ""
                else:
                    self.encoder.update()
            
            self.next_time += self.period