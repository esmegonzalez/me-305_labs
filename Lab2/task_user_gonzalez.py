'''
@file       task_user_gonzalez.py
@brief      User Interface Task
@details    Task that handles the user interface for the encoder that is given. When given commands it'll handle data communication.
@author     Esmeralda Gonzalez
@date       October 17, 2021 
'''

# imports
from Shared import Queue
import utime
import pyb

# STATES
S0_INIT = 0
S1_GET_COMMAND = 1
S2_COLLECT_DATA = 2
S3_PRINT_DATA = 3

class Task_User:
    def __init__(self, period, command, encoder):
        '''
        @brief      Initialize Function            
        @details    Initializes class with starting state, how often to update based on period, has two shared variables that is used to send commands to Encoder task.
        @param      period      The cycle period to update the encoder in microseconds
        @param      command     Command sent to Encoder task
        @param      encoder     The encoder object that is connected to the PINs on the board     
        '''

        # State of the User Task
        self.state = S0_INIT
        # Period time of updates
        self.period = period
        # Next time to update
        self.next_time = utime.ticks_us() + period
        # Serial communication port
        self.serial_com = None
        # When to stop collecting after collecting command
        self.stop_collecting_time = 0
        # Holds collecting data from collection command
        self.collection = Queue()

        self.command = command
        self.encoder = encoder
        
        
    def run(self):
        '''
        @brief      Periodic User Input Check
        @details    Periodicallys checks for user input to see if any commands have been given and will execute them as needed.
        '''

        # if the period window has been reached
        if utime.ticks_us() >= self.next_time:

            # intial setup state
            if self.state == S0_INIT:
                #RUN STATE 0

                # setup serial communication with USB
                self.serial_com = pyb.USB_VCP()

                print("------- Encoder program -------")
                print("")
                print("Following commands available:")
                print("\'z\' - Zero the position of the enconder")
                print("\'p\' - Print the position of the encoder")
                print("\'d\' - Print the delta of the enconder")
                print("\'g\' - Collect encoder data for 30 seconds")
                print("\'s\' - End collecting prematurely")
                print("")

                self.state == S1_GET_COMMAND
            
            # Wait for user to enter a command
            if self.state == S1_GET_COMMAND:
                if self.serial_com.any():
                    # command has been entered
                    char = self.serial_com.read(1)
                    # zero encoder command
                    if char == "z":
                        self.command.write(char)
                        print("Encoder has been set to zero.")
                    # print the position of encoder
                    elif char == "p":
                        print("Position of encoder: ", self.encoder.get_position())
                    # print delta of the encoder
                    elif char == "d":
                        print("Delta of encoder: ", self.encoder.get_delta())
                    # collect data from encoder for 30s
                    elif char == "g":
                        self.stop_collecting_time = utime.ticks_us() + 30000000
                        self.state = S2_COLLECT_DATA
            # collecting data state
            if self.state == S2_COLLECT_DATA:
                # if collecting stop time has not been reached
                if self.stop_collecting_time >= utime.ticks_us():
                    # add new position to collection of data
                    self.collection.add(self.encoder.get_position())
                    # check if the stop command has been entered during data collection
                    if self.serial_com.any():
                        char = self.serial_com.read(1)
                        if char == "s":
                            self.state = S3_PRINT_DATA
                # stop collecting data
                else:
                    self.state = S3_PRINT_DATA
            
            # print data that has been collected
            if self.state == S3_PRINT_DATA:
                temp = self.collection.next()
                
                # remove from collection queue until it is empty
                while temp is not None:
                    print(temp, end=",")
                    temp = self.collection.next()
                
                print("")
                print("")
                print("Done collecting and printing data.")
                self.state = S1_GET_COMMAND
 
            self.next_time += self.period